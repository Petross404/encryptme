#include <encryptme_facilities.h>

std::string encrypt_decrypt(std::string str_to_encrypt)
{
    char key = 'j';
    std::string output = str_to_encrypt;

    for (int i = 0; i < str_to_encrypt.size(); i++)
    {
        output[i] = str_to_encrypt[i] ^ key;
    }

    return output;
}

int main(int argc, char** argv)
{
    std::string to_be_encrypted, encrypted;
    fstream file("infile.txt");
        
    if (!file)
    {
	cout << "Create new file" << "\n";
	file.open("infile.txt", fstream::in | fstream::out | fstream::trunc);
    }
    
    if (!file.is_open())
    {
	std::cerr << "Abort";
	exit(-1);
    }
    
    if (file.peek() == EOF)
    {
	std::getline(cin, to_be_encrypted);
	encrypted = encrypt_decrypt(to_be_encrypted);
	std::cout << "Encypted : " << encrypted << "\n";
	file << encrypted;
    }
    
    std::string line;
    while (std::getline(file, line))
    {
	encrypted = encrypt_decrypt(line);
    }
    file << encrypted;
    
    file.close();
    
    return (0);
}
